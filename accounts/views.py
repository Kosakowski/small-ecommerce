import requests
from django.conf import settings
from django.contrib.auth import login, authenticate
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import CreateView, DetailView, UpdateView
from django.contrib import messages
from accounts import forms
from accounts import models


class SignupView(CreateView):
    template_name = 'registration/signup.html'
    form_class = forms.SignUpForm
    success_url = '/'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect(reverse('shop:list'))
        else:
            messages.error(request, "Proszę zaznaczyć polę 'Nie jestem robotem'.")
        return render(request, self.template_name, {'form': form})


