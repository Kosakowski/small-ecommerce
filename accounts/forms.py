from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Pole opcjonalne')
    last_name = forms.CharField(max_length=30, required=False, help_text='Pole opcjonalne')
    email = forms.EmailField(max_length=255, required=True, help_text='Pole wymagane')

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'password1', 'password2', )
