// A reference to Stripe.js initialized with a fake API key.
//Sign in to see examples pre-filled with your key.
var stripe = Stripe("pk_test_51H9zyIGE7AW5HPDxApa5d5HKdqQKil8ikwBtq3a1ZR5zbjQNXXuOqSOJZqtETBR9LmuxWrWcuigxnxk04cjvgn9S00ZBMUjHbK");
var elements = stripe.elements();

    var style = {
      base: {
        color: "#32325d",
        fontFamily: 'Arial, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#32325d"
        }
      },
      invalid: {
        fontFamily: 'Arial, sans-serif',
        color: "#fa755a",
        iconColor: "#fa755a"
      }
    };

    var card = elements.create("card", { style: style });
    // Stripe injects an iframe into the DOM
    card.mount("#card-element");

  // Handle real-time validation errors from the card Element.
  card.addEventListener('change', function(event) {
    var displayError = document.getElementById('card-errors');
    if (event.error) {
      displayError.textContent = event.error.message;
    } else {
      displayError.textContent = '';
    }
  });

//    card.on("change", function (event) {
//      // Disable the Pay button if there are no card details in the Element
//      document.querySelector("button").disabled = event.empty;
//      document.querySelector("#card-error").textContent = event.error ? event.error.message : "";
//    });


// Calls stripe.confirmCardPayment
// If the card requires authentication Stripe shows a pop-up modal to
// prompt the user to enter authentication details without leaving your page.
//var payWithCard = function(stripe, card, clientSecret) {
//  loading(true);
//  stripe
//    .confirmCardPayment(clientSecret, {
//      payment_method: {
//        card: card
//      }
//    })
//    .then(function(result) {
//      if (result.error) {
//        // Show error to your customer
//        showError(result.error.message);
//      } else {
//        // The payment succeeded!
//        orderComplete(result.paymentIntent.id);
//      }
//    });
//};

/* ------- UI helpers ------- */

// Shows a success message when the payment is complete
//var orderComplete = function(paymentIntentId) {
//  loading(false);
//  document
//    .querySelector(".result-message a")
//    .setAttribute(
//      "href",
//      "https://dashboard.stripe.com/test/payments/" + paymentIntentId
//    );
//  document.querySelector(".result-message").classList.remove("hidden");
//  document.querySelector("button").disabled = true;
//};
//
//// Show the customer the error from Stripe if their card fails to charge
//var showError = function(errorMsgText) {
//  loading(false);
//  var errorMsg = document.querySelector("#card-error");
//  errorMsg.textContent = errorMsgText;
//  setTimeout(function() {
//    errorMsg.textContent = "";
//  }, 4000);
//};

  // Handle form submission.
  var form = document.getElementById('payment-form');
  form.addEventListener('submit', function(event) {
    event.preventDefault();

    stripe.createToken(card).then(function(result) {
      if (result.error) {
        // Inform the user if there was an error.
        var errorElement = document.getElementById('card-errors');
        errorElement.textContent = result.error.message;
      } else {
        // Send the token to your server.
        stripeTokenHandler(result.token);
      }
    });
  });

  // Submit the form with the token ID.
  function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    var form = document.getElementById('payment-form');
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);

    // Submit the form
    form.submit();
  }
