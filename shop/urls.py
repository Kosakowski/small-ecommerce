from django.urls import path, include
from .views import Home, ItemDetailView, add_to_cart, remove_from_cart, remove_single_item_from_cart, OrderSummaryView,\
    CheckoutView, PaymentView, AboutView, CategoryDetailView, UserSummary

app_name = 'shop'

urlpatterns = [
    path('', Home.as_view(), name='list'),
    path('product/<slug>/', ItemDetailView.as_view(), name='product'),
    path('category/<slug>/', CategoryDetailView.as_view(), name='category'),
    path('order-summary/', OrderSummaryView.as_view(), name='order-summary'),
    path('add-to-cart/<slug>/', add_to_cart, name='add-to-cart'),
    path('remove-from-cart/<slug>/', remove_from_cart, name='remove-from-cart'),
    path('remove-single-item-from-cart/<slug>/', remove_single_item_from_cart, name='remove-single-item-from-cart'),
    path('checkout/', CheckoutView.as_view(), name='checkout'),
    path('payment/<payment_option>/', PaymentView.as_view(), name='payment'),
    path('about/', AboutView.as_view(), name='about'),
    path('user-summary/', UserSummary.as_view(), name='user-summary'),
]
