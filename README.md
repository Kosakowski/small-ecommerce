Small E-commerce  

The idea behind the project was to build a small online store.   
The payment mockup is made in Stripe.   

The repository contains a database and media files, so you can run the project and view it in full (yes, I know it's not good practice, it's just for demonstration purposes).
