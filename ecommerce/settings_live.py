from .settings_base import *

DEBUG = False
ALLOWED_HOSTS = ['ecommerce.mkos.usermd.net']

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'host'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'user_host'
EMAIL_HOST_PASSWORD = 'user_pass'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'test@test.com'

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

STATIC_ROOT = os.path.join(BASE_DIR, 'public', 'static')
MEDIA_ROOT = os.path.join(BASE_DIR, 'public', 'media')
